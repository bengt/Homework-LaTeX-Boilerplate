MODULE = AB1
HOMEWORK = 1

all: $(MODULE)_$(HOMEWORK).tex
	latex --output-format=pdf $(MODULE)_$(HOMEWORK).tex
	make clean
clean:
	rm -f $(MODULE)_$(HOMEWORK).log $(MODULE)_$(HOMEWORK).aux
clean-all:
	rm -f $(MODULE)_$(HOMEWORK).log $(MODULE)_$(HOMEWORK).aux $(MODULE)_$(HOMEWORK).pdf $(MODULE)_$(HOMEWORK).dvi
